library(shiny)
library(DT)
library(plotly)
library(crosstalk)
library(highcharter) 

# get data
m <- pokemon

# Define UI for application that draws a histogram
ui <- navbarPage("Pokemon!",
                       
          tabPanel("Table",
                   fluidPage(
                      DT::dataTableOutput("x1")
)
)
)

# Define server logic required to draw a histogram
server <- function(input, output) {
  
  output$x1 <- renderDT(
    m %>% select(id,pokemon,species_id,height,weight,base_experience, type_1,type_2),
    options = list(lengthChange = FALSE),
    rownames = FALSE,
    selection = 'single')
  
  observeEvent(input$x1_rows_selected, {
    showModal(modalDialog(
      title = "A wild chart appears",
      navbarPage(m$pokemon[input$x1_rows_selected],
                 
                 tabPanel("Plot One",

      renderPlotly({
        s <- input$x1_rows_selected
        d <- m[s,]
        p <- d %>% select(attack, defense, hp, special_attack, special_defense, speed)
        
        b_theta <- colnames(p)
        b_r <- as.numeric(p[1,])
        
        coloring <- d %>% select(color_1)
        b_color <- as.character(coloring[1,])
        
        plotz <- plot_ly(
          type = 'scatterpolar',
          r = b_r,
          theta = b_theta,
          fill = 'toself',
          fillcolor = b_color, 
          opacity = 0.8,
          marker = list(color = b_color)
        ) %>%
          layout(
            polar = list(
              radialaxis = list(
                visible = T,
                range = c(0,255)
              )
            ),
            showlegend = F
          )
        plotz
      })
      ),
      tabPanel("Plot Two",
               
               renderPlotly({
                 s <- input$x1_rows_selected
                 d <- m[s,]
                 type <- d$type_1
                 a <- m %>% filter(type_1 == type) %>% select(attack, defense, type_2)
                
                 plot2 <- plot_ly(
                   data = a, x = ~attack, y = ~defense, color = ~type_2
                 ) 

                 plot2
               })

      )
                 )
      ,
      easyClose = TRUE
    ))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)

